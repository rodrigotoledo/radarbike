// MAIN.JS
//--------------------------------------------------------------------------------------------------------------------------------
//This is main JS file that contains custom JS scipts and initialization used in this template*/
// -------------------------------------------------------------------------------------------------------------------------------
// Template Name: Mountain Bike - Sport, Bmx, Bicycle Html Template.
// Author: Iwthemes.
// Version 1 - may 2015
// Website: http://www.iwthemes.com
// Email: support@iwthemes.com
// Copyright: (C) 2015
// -------------------------------------------------------------------------------------------------------------------------------

$(document).ready(function(){

   "use strict";
//=============Loader===============================//
  jQuery(window).load(function() {
    jQuery(".status").fadeOut();
    jQuery(".preloader").delay(500).fadeOut("slow");
  })


//==============Totop==============================//
  $().UItoTop({
    scrollSpeed:500,
    easingType:'linear'
  });


//==============Login=============================//
  $('#showlogin').click(function() {
          $('#loginpanel').slideToggle('slow', function() {
           $("#triangle_down").toggle();
        $("#triangle_up").toggle();
    });
  });


//========Nav Responsive===========================//
  $('#menu').tinyNav({
      active: 'selected'
  });
  $('ul.sf-menu').superfish();


//=========Popover & Tooltip======================//
$('[data-toggle="popover"]').popover()
 if( $.fn.tooltip() ) {
    $('[class="tooltip"]').tooltip();
  }


//=========Slider Responsive======================//
  $(".rslides").responsiveSlides();


//================Twitter Feed===================//
  $(".twitter").tweet({
      modpath: 'js/twitter/index.php',
      username: "envato",
      count: 5,
      loading_text: "Loading tweets...",
  });

//==========Carousel Bike Home===================//
    $('.carousel-bikes.home').owlCarousel({
        loop:true,
        margin:25,
        nav:false,
        autoplay: true,
        autoplayHoverPause: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })

//==========Carousel Bike ======================//
   $('.carousel-bikes').owlCarousel({
      loop:true,
      margin:25,
      nav:false,
      autoplay: true,
      autoplayHoverPause: true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:4
          }
      }
  })

//==========List Bike =========================//
  $('.list-bikes.carousel').owlCarousel({
      margin:25,
      nav:false,
      autoplayTimeout: 2000,
      autoplay: true,
      autoplayHoverPause: true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:2
          },
          1024:{
              items:4
          }
      }
  })

//==========Gallery Images ====================//
  $('.gallery-images').owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      autoplay: true,
      autoplayHoverPause: true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:5
          },
          1000:{
              items:4
          }
      }
  })

//==========Team Carousel ====================//
  $('.team-carousel').owlCarousel({
      loop:true,
      margin:3,
      nav:false,
      autoplay: true,
      autoplayHoverPause: true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:2
          }
      }
  })

//===============Tweet List=================//
  $('.tweet_list').owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      autoplay: true,
      responsive:{
          1000:{
              items:1
          }
      }
  })

//===========Quotation Gallery==============//
  $('.quotation-gallery').owlCarousel({
    loop:false,
    margin:15,
    nav:false,
    autoplay: true,
    autoplayHoverPause: true,
    responsive:{
        0:{
            items:1
        },
        800:{
            items:2
        },
        1000:{
            items:4
        }
    }
  })

//===========Coming Soon Timer==============//
  $('#coming-soon').countdown('2015/08/20', function(event) {
    var $this = $(this).html(event.strftime(''
    + '<span>%D : <br> <small>Dias </small></span>  '
    + '<span>%H : <br> <small>Hr</small> </span>  '
    + '<span>%M : <br> <small>Min</small> </span>  '
    + '<span>%S <br> <small>Seg</small></span> '));
  });

//===========Parallax Efect===============//
    $('.bg_parallax').parallax("50%", .12);

//===========Smooth Scroll===============//
  var scrollAnimationTime = 1200,
      scrollAnimation = 'easeInOutExpo';
  $('a.scrollto').bind('click.smoothscroll', function (event) {
      event.preventDefault();
      var target = this.hash;
      $('html, body').stop().animate({
          'scrollTop': $(target).offset().top
      }, scrollAnimationTime, scrollAnimation, function () {
          window.location.hash = target;
      });
  });

//================Lightbox===============//
    $(".iframe_video").fancybox({
      'width'       : '65%',
      'height'      : '75%',
      'autoScale'   : false,
      'transitionIn': 'none',
      'transitionOut': 'none',
      'type'        : 'iframe'
    });


      jQuery("a[class*=fancybox]").fancybox({
      'overlayOpacity'  : 0.7,
      'overlayColor'    : '#000000',
      'transitionIn'    : 'elastic',
      'transitionOut'   : 'elastic',
      'easingIn'        : 'easeOutBack',
      'easingOut'       : 'easeInBack',
      'speedIn'       : '700',
      'centerOnScroll'  : true,
      'titlePosition'     : 'over'
    });

//=========Animations===================//
    new WOW().init();

//=================================== Subtmit Form  ====================================//
    // $('.form-contact').submit(function(event) {
    //   event.preventDefault();
    //   var url = $(this).attr('action');
    //   var datos = $(this).serialize();
    //   $.get(url, datos, function(resultado) {
    //     $('.result').html(resultado);
    //   });
    // });

//=========Newsletter===================//
  // $('#newsletterForm').submit(function(event) {
  //   event.preventDefault();
  //   var url = $(this).attr('action');
  //   var datos = $(this).serialize();
  //    $.get(url, datos, function(resultado) {
  //     $('#result-newsletter').html(resultado);
  //   });
  // });

//=========flickr======================//
    $('#flickr').jflickrfeed({
    limit: 8, //Number of images to be displayed
    qstrings: {
      id: '36587311@N08'//Change this to any Flickr Set ID as you prefer.
    },
    itemTemplate: '<li><a href="{{image_b}}" class="fancybox"><img src="{{image_s}}" alt="{{title}}" /></a></li>'
  });

});

//======Slider Revolution==============//
  jQuery('.tp-banner').show().revolution(
    {
        navigationType:"none",
        navigationArrows:"solo",
        navigationStyle:"preview4",

        keyboardNavigation:"off",

        navigationHAlign:"center",
        navigationVAlign:"bottom",
        navigationHOffset:0,
        navigationVOffset:20,

        soloArrowLeftHalign:"left",
        soloArrowLeftValign:"center",
        soloArrowLeftHOffset:20,
        soloArrowLeftVOffset:0,

        soloArrowRightHalign:"right",
        soloArrowRightValign:"center",
        soloArrowRightHOffset:20,
        soloArrowRightVOffset:0,

        delay:9000,
        startwidth:1170,
        startheight:800,
        hideThumbs:10,
        fullWidth:"on",
        forceFullWidth:"on"
    });
