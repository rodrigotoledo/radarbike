class ProductBikePart < ActiveRecord::Base
  belongs_to :product
  belongs_to :bike_part
end
