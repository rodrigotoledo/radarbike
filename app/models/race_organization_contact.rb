class RaceOrganizationContact
  include ActiveModel::Model

  attr_accessor :name, :visitor_email, :message, :subject, :occur_at, :motive

  validates_presence_of :name, :message, :visitor_email, :occur_at, :motive
  validates :visitor_email, email: true
  def deliver
    return false unless self.valid?
    message = []
    message << "Motivo: #{self.motive}"
    message << "Data: #{I18n.l(self.occur_at.to_date, format: :long)}"
    message << ""
    message << "Descrição:"
    message << ""
    message << self.message
    ContactMailer.new_contact_of_race_organization(self.name, self.visitor_email, self.subject, message.join("\n\n")).deliver
  end
end
