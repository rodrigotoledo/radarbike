class Product < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  alias_attribute :title, :name
  attr_accessor :product_picture

  belongs_to :user
  has_many :pictures, as: :imageable, dependent: :destroy

  validates :name, :description, :product_type, :features, presence: true
  validates :product_picture, presence: true, on: :create

  scope :actives, -> { where(active: true) }

  def picture
    @picture ||= begin
      product_picture = pictures.where(master: true).first
      product_picture.try(:photo)
    end
  end

  def contact_data
    @contact_data = {}
    @contact_data[:contact_name] = self.contact_name unless self.contact_name.blank?
    @contact_data[:contact_email] = self.contact_email unless self.contact_email.blank?
    @contact_data[:contact_phone] = self.contact_phone unless self.contact_phone.blank?
    @contact_data[:contact_facebook] = self.contact_facebook unless self.contact_facebook.blank?
    @contact_data
  end

  before_save do
    unless self.user_id.blank?
      self.contact_name = self.user.name
      self.contact_email = self.user.email
    end
  end

  after_save do
    if self.product_picture
      Picture.transaction do
        self.pictures.update_all(master: false)
        self.pictures.build(master: true, photo: self.product_picture).save!
      end
    end
  end

  def categories
    self.product_type.split(',').map(&:strip)
  end

  def colors_list
    self.colors = 'black' if self.colors.blank?
    self.colors.to_s.split(',').map(&:strip)
  end

  def features_list
    self.features = '' if self.features.blank?
    self.features.to_s.split("\n").map(&:strip)
  end
end
