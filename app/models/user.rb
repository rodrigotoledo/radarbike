class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook, :twitter]

  validates :name, presence: true

  has_many :products, dependent: :nullify
  has_many :races, dependent: :nullify
  has_many :posts, dependent: :nullify
  has_many :robberies, dependent: :nullify

  has_attached_file :photo, styles: { medium: "600x800#", thumb: "80x80>", gallery_list: "176x118#", big: "1024x768#" }
  validates_attachment_content_type :photo, content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  def self.invite_or_resend(email)
    user = User.where(email: email).first
    if user && user.invitation_accepted?
      return true
    else
      if user
        user.deliver_invitation
      else
        User.invite!(email: email, name: email)
      end
    end
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   # assuming the user model has a name
      user.photo = process_uri(auth.info.image) # assuming the user model has an image
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  after_create do
    if self.send_races
      Race.where("occur_at >= ? and send_notifications = ?", Date.today, true).order(occur_at: :desc).each do |race|
        RaceMailer.delay.new_race(self, race)
      end
    end
  end

  private

  def self.process_uri(uri)
    require 'open-uri'
    require 'open_uri_redirections'
    open(uri, :allow_redirections => :safe) do |r|
      r.base_uri.to_s
    end
  end
end
