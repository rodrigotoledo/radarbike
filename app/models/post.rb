class Post < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  attr_accessor :post_picture

  belongs_to :user
  has_many :post_post_categories
  has_many :post_categories, through: :post_post_categories
  has_many :pictures, as: :imageable, dependent: :destroy

  scope :actives, -> {where(active: true)}
  scope :ordered, -> {order(published_at: :desc)}
  validates :user_id, :title, :resume, :content, :published_at, presence: true
  validates :post_picture, presence: true, on: :create


  def picture
    @picture ||= begin
      post_picture = pictures.where(master: true).first
      post_picture.try(:photo)
    end
  end

  after_save do
    if self.post_picture
      Picture.transaction do
        self.pictures.update_all(master: false)
        self.pictures.build(master: true, photo: self.post_picture).save!
      end
    end
  end
end
