class Race < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  attr_accessor :race_picture

  belongs_to :user
  has_many :pictures, as: :imageable, dependent: :destroy

  validates :user_id, :title, :resume, :description, :contact, :contact_phone, :race_details,
  :occur_at, :registration_start_at, :registration_end_at, :zipcode, :state, :city, :address,
  :payment_method, :payment_details, presence: true
  validates :race_picture, presence: true, on: :create

  def picture
    @picture ||= begin
      race_picture = pictures.where(master: true).first
      race_picture.try(:photo)
    end
  end

  after_save do
    if self.race_picture
      if race_picture_ziped?
        save_race_pictures_in_zip
      else
        save_race_picture_image
      end
    end
  end

  after_create do
    if self.send_notifications && !self.occured?
      User.where(send_races: true).each do |user|
        RaceMailer.delay.new_race(user, self)
      end
    end
  end

  def notify!
    User.where(send_races: true).each do |user|
      RaceMailer.delay.new_race(user, self)
    end
  end

  def occured?
    self.occur_at < Date.today
  end

  def race_picture_ziped?
    File.extname(self.race_picture.path).downcase.include?("zip")
  end

  def save_race_picture_image
    Picture.transaction do
      self.pictures.update_all(master: false)
      self.pictures.build(master: true, photo: self.race_picture).save!
    end
  end

  def save_race_pictures_in_zip
    require 'zip'
    Zip::File.open(self.race_picture.path) do |zipfile|
      zipfile.each do |file|
        next if file.directory? || File.basename(file.name)[0..1] == "._"
        tempfile_path = Rails.root.join('tmp', File.basename(file.name))
        zipfile.extract(file, tempfile_path.to_s) unless File.exist?(tempfile_path)

        picture = self.pictures.build(photo: File.open(tempfile_path), master: false)
        picture.save(validate: false)

        File.unlink(tempfile_path)
      end
    end
  end
end
