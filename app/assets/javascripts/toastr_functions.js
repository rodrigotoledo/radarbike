toastr.options = {
  "newestOnTop": true,
  "closeButton": true,
  "debug": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "onclick": null,
  "showDuration": "1000",
  "hideDuration": "5000",
  "timeOut": false,
  "extendedTimeOut": "0",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

jQuery(document).ready(function(){
  $('div.messages').each(function(){
    var type = $(this).data('type');
    if (type == 'notice') {
      type = 'success'
    }else if(type == "alert"){
      type = 'error'
    }
    eval("toastr."+type+"('"+$(this).html()+"')")
  })
})
