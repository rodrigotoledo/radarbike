// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery_nested_form
//= require template/nav/tinynav
//= require template/nav/hoverIntent
//= require template/nav/superfish
//= require template/nav/jquery.sticky
//= require template/fancybox/jquery.fancybox
//= require template/animations/wow
//= require template/totop/jquery.ui.totop
//= require template/carousel/carousel
//= require template/counter/jquery.countdown
//= require template/maps/gmap3
//= require template/slide-revolution/jquery.themepunch.tools.min
//= require template/slide-revolution/jquery.themepunch.revolution
//= require template/slide/responsiveslides
//= require template/parallax/jquery.inview
//= require template/parallax/nbw-parallax
//= require template/twitter/jquery.tweet
//= require template/flickr/jflickrfeed
//= require template/menu/jquery.nav
//= require template/bootstrap/bootstrap
//= require template/theme-options/theme-options
//= require template/theme-options/jquery.cookies
//= theme-options/jquery.easing.1.3.min
//= require toastr
//= require toastr_functions
//= require template/main
//= require functions
