class ContactController < ApplicationController
  def index
    @contact = Contact.new
  end

  def send_contact
    @contact = Contact.new(contact_params)
    if @contact.deliver
      redirect_to :index, success: 'Mensagem enviada com sucesso. Em breve entraremos em contato.'
    else
      flash[:error] = 'Falha ao enviar a mensagem. Todos os campos são obrigatórios'
      render :index
    end
  end

  private
    def contact_params
      params[:contact].permit(:name, :visitor_email, :subject, :message)
    end
end
