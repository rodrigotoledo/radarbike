class RacesController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_race, only: [:show, :edit, :update, :destroy, :send_notifications]

  # GET /races
  # GET /races.json
  def index
    @races = Race.order(occur_at: :desc)
  end

  # GET /races/1
  # GET /races/1.json
  def show
  end

  # GET /races/new
  def new
    @race = Race.new
    10.times do
      @race.pictures.build
    end
  end

  # GET /races/1/edit
  def edit
  end

  # POST /races
  # POST /races.json
  def create
    @race = Race.new(race_params)

    respond_to do |format|
      if @race.save
        format.html { redirect_to public_race_path(@race), notice: 'Race was successfully created.' }
        format.json { render :show, status: :created, location: @race }
      else
        format.html { render :new }
        format.json { render json: @race.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /races/1
  # PATCH/PUT /races/1.json
  def update
    respond_to do |format|
      if @race.update(race_params)
        format.html { redirect_to public_race_path(@race), notice: 'Race was successfully updated.' }
        format.json { render :show, status: :ok, location: @race }
      else
        format.html { render :edit }
        format.json { render json: @race.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /races/1
  # DELETE /races/1.json
  def destroy
    @race.destroy
    respond_to do |format|
      format.html { redirect_to races_url, notice: 'Race was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def send_notifications
    @race.notify!
    redirect_to :back, notice: 'Notificações sendo enviadas'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_race
      @race = Race.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def race_params
      params[:race].permit(:user_id, :title, :resume, :description, :race_type, :organization,
        :contact, :contact_phone, :contact_mobile, :contact_email, :race_details, :occur_at,
        :registration_start_at, :registration_end_at, :state, :city, :address, :zipcode, :video,
        :payment_method, :payment_details, :race_picture, :show_title_in_index, :send_notifications)
    end
end
