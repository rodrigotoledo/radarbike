class PublicTeamsController < ApplicationController
  before_action :set_team, only: [:show]

  def index
    @teams = Team.order("rand()")
  end

  def special
    @teams = Team.where(special: true).order("rand()")
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.friendly.find(params[:id])
    end
end
