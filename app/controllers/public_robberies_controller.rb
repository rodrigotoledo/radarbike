class PublicRobberiesController < ApplicationController
  before_action :set_robbery, only: [:show]

  def new
    @robbery = Robbery.new
    @robbery.robbery_bike_parts.build
  end

  def create
    @robbery = Robbery.new(robbery_params)
    @robbery.user_id = current_user.id if user_signed_in?

    respond_to do |format|
      if @robbery.save
        format.html { redirect_to @robbery, notice: 'Robbery was successfully created.' }
        format.json { render :show, status: :created, location: @robbery }
      else
        format.html { render :new }
        format.json { render json: @robbery.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /robberys/1
  # GET /robberys/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_robbery
      @robbery = current_user.robberies.find(params[:id])
    end

    def robbery_params
      params.require(:robbery).permit(:description, :photo, :details, :zipcode, :state, :city,
        :address, :is_public,
        :contact_name, :contact_email, :contact_phone, :contact_facebook,
        robbery_bike_parts_attributes: [:id, :part_name, :part_value, :_destroy])
    end
end
