class PicturesController < ApplicationController
  before_action :authenticate_admin!
  helper_method :author_gallery
  before_action :set_picture, only: [:show, :edit, :update, :destroy, :make_master]

  # GET /races
  # GET /races.json
  def index
    @pictures = author_gallery.pictures.order("master, id")
  end

  # GET /races/1
  # GET /races/1.json
  def show
  end

  # GET /races/new
  def new
    @race = Race.new
    10.times do
      @race.pictures.build
    end
  end

  # GET /races/1/edit
  def edit
  end

  # POST /races
  # POST /races.json
  def create
    @picture = author_gallery.pictures.build(picture_params)

    respond_to do |format|
      if @picture.save
        if @picture.master
          author_gallery.pictures.where("id <> ?", @picture.id).update_all(master: false)
        end
        format.html { redirect_to :back, notice: 'Race was successfully created.' }
        format.json { render :show, status: :created, location: @race }
      else
        format.html { redirect_to :back, error: 'Erro ao criar imagem.' }
        format.json { render json: @race.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /races/1
  # PATCH/PUT /races/1.json
  def update
    respond_to do |format|
      if @race.update(picture_params)
        format.html { redirect_to public_race_path(@race), notice: 'Race was successfully updated.' }
        format.json { render :show, status: :ok, location: @race }
      else
        format.html { render :edit }
        format.json { render json: @race.errors, status: :unprocessable_entity }
      end
    end
  end

  def make_master
    author_gallery.pictures.update_all(master: false)
    @picture.update_attribute(:master, true)
    redirect_to :back, notice: 'Imagem principal modificada com sucesso'
  end

  # DELETE /races/1
  # DELETE /races/1.json
  def destroy
    @picture.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Race was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def author_gallery
      @author_gallery ||= begin
        klass = [Race, Post, Product, Team].detect{|c| params["#{c.name.underscore}_id"]}
        klass.friendly.find(params["#{klass.name.underscore}_id"])
      end
    end

    def set_picture
      @picture = author_gallery.pictures.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def picture_params
      params.require(:picture).permit(:photo, :master)
    end
end
