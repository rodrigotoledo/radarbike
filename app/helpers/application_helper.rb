module ApplicationHelper
  def title_for_create_update(operation, model_name)
    title = t(operation, model_name: t("activerecord.models.#{model_name}")).html_safe
    content_for(:title) do
      strip_tags(title)
    end
    title
  end

  def title_for_list(model_name)
    title = t(:list, model_name: t("activerecord.models.#{model_name}")).html_safe
    content_for(:title) do
      strip_tags(title)
    end
    title
  end

  def flash_messages(format = :html)
    case format
    when :html
      return flash_messages_html
    else
      return flash_messages_js
    end
  end

  def flash_messages_html
    flash.inject('') do |html, values|
      key, message = values
      html << content_tag(:div, message, class: "alert alert-#{key}")
    end.html_safe
  end

  def flash_messages_js
    flash.inject('') do |html, values|
      key, message = values
      html << content_tag(:div, message, class: "messages", data: {type: key}, style: "display:none")
    end.html_safe
  end
end
