module PostsHelper
  def post_categories_sentence(post)
    post.post_categories.map{|t| link_to(t.name, public_post_category_path(t))}.to_sentence.html_safe
  end
end
