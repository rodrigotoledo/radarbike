class CreateRaceVideos < ActiveRecord::Migration
  def change
    create_table :race_videos do |t|
      t.attachment :video
      t.references :race, index: true, foreign_key: true
      t.boolean :master

      t.timestamps null: false
    end
  end
end
