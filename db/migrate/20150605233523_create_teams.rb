class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.attachment :photo
      t.text :description
      t.boolean :special

      t.timestamps null: false
    end
  end
end
