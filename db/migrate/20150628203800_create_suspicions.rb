class CreateSuspicions < ActiveRecord::Migration
  def change
    create_table :suspicions do |t|
      t.string :description
      t.string :zipcode
      t.string :state
      t.string :city
      t.string :address
      t.attachment :photo
      t.string :url

      t.timestamps null: false
    end
  end
end
