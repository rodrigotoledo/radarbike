class AddSlugToRaces < ActiveRecord::Migration
  def change
    add_column :races, :slug, :string
  end
end
