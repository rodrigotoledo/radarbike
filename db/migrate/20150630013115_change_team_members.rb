class ChangeTeamMembers < ActiveRecord::Migration
  def up
    create_table :team_members, force: true do |t|
      t.references :team, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  def down
    create_table :team_members, force: true do |t|
      t.references :team, index: true, foreign_key: true
      t.string :member
      t.text :resume

      t.timestamps null: false
    end
  end
end
