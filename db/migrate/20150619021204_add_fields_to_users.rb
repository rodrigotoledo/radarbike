class AddFieldsToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.references :team
      t.string :name, null: false
      t.date :birthday
      t.attachment :photo, null: false
      t.string :facebook_url
      t.string :strava_id
      t.text :info
    end
  end
end
