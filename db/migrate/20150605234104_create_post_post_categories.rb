class CreatePostPostCategories < ActiveRecord::Migration
  def change
    create_table :post_post_categories do |t|
      t.references :post, index: true, foreign_key: true
      t.references :post_category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
