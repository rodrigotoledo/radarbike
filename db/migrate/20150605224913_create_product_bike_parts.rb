class CreateProductBikeParts < ActiveRecord::Migration
  def change
    create_table :product_bike_parts do |t|
      t.references :product, index: true, foreign_key: true
      t.references :bike_part, index: true, foreign_key: true
      t.string :part_value

      t.timestamps null: false
    end
  end
end
