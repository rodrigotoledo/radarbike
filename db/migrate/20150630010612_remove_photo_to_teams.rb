class RemovePhotoToTeams < ActiveRecord::Migration
  def change
    remove_column :teams, :photo_file_name
    remove_column :teams, :photo_content_type
    remove_column :teams, :photo_file_size
    remove_column :teams, :photo_updated_at
  end
end
