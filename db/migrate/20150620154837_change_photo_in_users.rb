class ChangePhotoInUsers < ActiveRecord::Migration
  def change
    change_column :users, :photo_file_name, :string, null: true
    change_column :users, :photo_file_size, :integer, null: true
    change_column :users, :photo_content_type, :string, null: true
    change_column :users, :photo_updated_at, :datetime, null: true
  end
end
