class CreateRaces < ActiveRecord::Migration
  def change
    create_table :races do |t|
      t.references :user, index: true, foreign_key: true
      t.string :title
      t.text :resume
      t.text :description
      t.string :race_type
      t.string :organization
      t.string :contact
      t.string :contact_phone
      t.string :contact_mobile
      t.string :contact_email
      t.text :race_details
      t.datetime :occur_at
      t.datetime :registration_start_at
      t.datetime :registration_end_at
      t.string :state
      t.string :city
      t.string :address
      t.string :zipcode
      t.string :payment_method
      t.text :payment_details

      t.timestamps null: false
    end
  end
end
