class AddUserIdToRobberies < ActiveRecord::Migration
  def change
    add_column :robberies, :user_id, :integer
    add_index :robberies, :user_id
  end
end
