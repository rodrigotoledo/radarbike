class AddContactsToProducts < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.string :contact_name
      t.string :contact_email
      t.string :contact_phone
      t.string :contact_facebook
    end
  end
end
