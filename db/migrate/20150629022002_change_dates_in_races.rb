class ChangeDatesInRaces < ActiveRecord::Migration
  def change
    change_column :races, :registration_start_at, :date
    change_column :races, :registration_end_at, :date
  end
end
