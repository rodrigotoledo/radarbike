class AddSendRacesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :send_races, :boolean, default: true
  end
end
