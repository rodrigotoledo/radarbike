class AddSendNotificationsToRaces < ActiveRecord::Migration
  def change
    add_column :races, :send_notifications, :boolean, default: true
  end
end
