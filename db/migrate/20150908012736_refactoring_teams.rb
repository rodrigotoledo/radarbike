class RefactoringTeams < ActiveRecord::Migration
  def change
    create_table :team_members, force: true do |t|
      t.references :team, index: true, foreign_key: true
      t.string :member
      t.text :resume
      t.attachment :photo

      t.timestamps null: false
    end
  end
end
